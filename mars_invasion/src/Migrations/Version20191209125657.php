<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191209125657 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE minerals (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, effects_on_population LONGTEXT NOT NULL, threat_evaluation SMALLINT NOT NULL, icon VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE minerals_areas (minerals_id INT NOT NULL, areas_id INT NOT NULL, INDEX IDX_330930CD5882561 (minerals_id), INDEX IDX_330930C1E756D0A (areas_id), PRIMARY KEY(minerals_id, areas_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roles (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categories (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, special_traits LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE earthlings (id INT AUTO_INCREMENT NOT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, age INT DEFAULT NULL, genetic_fingerprint VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE earthlings_categories (earthlings_id INT NOT NULL, categories_id INT NOT NULL, INDEX IDX_4120435171048821 (earthlings_id), INDEX IDX_41204351A21214B7 (categories_id), PRIMARY KEY(earthlings_id, categories_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE earthlings_roles (earthlings_id INT NOT NULL, roles_id INT NOT NULL, INDEX IDX_62838F5C71048821 (earthlings_id), INDEX IDX_62838F5C38C751C4 (roles_id), PRIMARY KEY(earthlings_id, roles_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE areas (id INT AUTO_INCREMENT NOT NULL, sectors_id INT NOT NULL, s_col_start SMALLINT NOT NULL, s_col_end SMALLINT NOT NULL, s_row_start SMALLINT NOT NULL, s_row_end SMALLINT NOT NULL, name VARCHAR(255) NOT NULL, is_safe TINYINT(1) NOT NULL, has_ressources TINYINT(1) DEFAULT NULL, ground_type VARCHAR(255) DEFAULT NULL, has_been_investigated TINYINT(1) NOT NULL, risk SMALLINT NOT NULL, INDEX IDX_58B0B25C3479DC16 (sectors_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sectors (id INT AUTO_INCREMENT NOT NULL, number INT NOT NULL, col_start SMALLINT NOT NULL, col_end SMALLINT NOT NULL, row_start SMALLINT NOT NULL, row_end SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reports (id INT AUTO_INCREMENT NOT NULL, earthlings_id INT NOT NULL, areas_id INT NOT NULL, date DATETIME NOT NULL, title VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, validated_by_scientist TINYINT(1) NOT NULL, INDEX IDX_F11FA74571048821 (earthlings_id), INDEX IDX_F11FA7451E756D0A (areas_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE minerals_areas ADD CONSTRAINT FK_330930CD5882561 FOREIGN KEY (minerals_id) REFERENCES minerals (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE minerals_areas ADD CONSTRAINT FK_330930C1E756D0A FOREIGN KEY (areas_id) REFERENCES areas (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE earthlings_categories ADD CONSTRAINT FK_4120435171048821 FOREIGN KEY (earthlings_id) REFERENCES earthlings (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE earthlings_categories ADD CONSTRAINT FK_41204351A21214B7 FOREIGN KEY (categories_id) REFERENCES categories (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE earthlings_roles ADD CONSTRAINT FK_62838F5C71048821 FOREIGN KEY (earthlings_id) REFERENCES earthlings (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE earthlings_roles ADD CONSTRAINT FK_62838F5C38C751C4 FOREIGN KEY (roles_id) REFERENCES roles (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE areas ADD CONSTRAINT FK_58B0B25C3479DC16 FOREIGN KEY (sectors_id) REFERENCES sectors (id)');
        $this->addSql('ALTER TABLE reports ADD CONSTRAINT FK_F11FA74571048821 FOREIGN KEY (earthlings_id) REFERENCES earthlings (id)');
        $this->addSql('ALTER TABLE reports ADD CONSTRAINT FK_F11FA7451E756D0A FOREIGN KEY (areas_id) REFERENCES areas (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE minerals_areas DROP FOREIGN KEY FK_330930CD5882561');
        $this->addSql('ALTER TABLE earthlings_roles DROP FOREIGN KEY FK_62838F5C38C751C4');
        $this->addSql('ALTER TABLE earthlings_categories DROP FOREIGN KEY FK_41204351A21214B7');
        $this->addSql('ALTER TABLE earthlings_categories DROP FOREIGN KEY FK_4120435171048821');
        $this->addSql('ALTER TABLE earthlings_roles DROP FOREIGN KEY FK_62838F5C71048821');
        $this->addSql('ALTER TABLE reports DROP FOREIGN KEY FK_F11FA74571048821');
        $this->addSql('ALTER TABLE minerals_areas DROP FOREIGN KEY FK_330930C1E756D0A');
        $this->addSql('ALTER TABLE reports DROP FOREIGN KEY FK_F11FA7451E756D0A');
        $this->addSql('ALTER TABLE areas DROP FOREIGN KEY FK_58B0B25C3479DC16');
        $this->addSql('DROP TABLE minerals');
        $this->addSql('DROP TABLE minerals_areas');
        $this->addSql('DROP TABLE roles');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE earthlings');
        $this->addSql('DROP TABLE earthlings_categories');
        $this->addSql('DROP TABLE earthlings_roles');
        $this->addSql('DROP TABLE areas');
        $this->addSql('DROP TABLE date');
        $this->addSql('DROP TABLE sectors');
        $this->addSql('DROP TABLE reports');
    }
}
