<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Areas;
use App\Entity\Sectors;


class AreasFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $sectorRepo = $this->getDoctrine()->getRepository(Sectors::class);
        $sector1= $sectorRepo->find(1);


        $area1 = new Areas();

        $area1->setSColStart(1);
        $area1->setSColEnd(2);
        $area1->setName("Death Canyon") ;
        $area1->setSRowStart(1) ;
        $area1->setSRowEnd(2) ;
        $area1->setIsSafe(false) ;
        $area1->setHasRessources(true) ;
        $area1->setHasBeenInvestigated(true) ;
        $area1->setGroundType("mountain") ;
        $area1->setRisk(10) ;
        $area1->setSectors($sector1);

        $manager->persist($area1);    

        $area2 = new Areas();

        $area2->setSColStart(2);
        $area2->setSColEnd(3);
        $area2->setName("Teletobies Land") ;
        $area2->setSRowStart(1) ;
        $area2->setSRowEnd(2) ;
        $area2->setIsSafe(true) ;
        $area2->setHasRessources(false) ;
        $area2->setHasBeenInvestigated(false) ;
        $area2->setGroundType("valley") ;
        $area2->setRisk(1) ;        
        $area2->setSectors($sector1);



        $manager->persist($area2);  
       
       
       
       
       
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
