<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Sectors;


class SectorsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $sector1 = new Sectors();

        $sector1->setNumber(1);
        $sector1->setColStart(1);
        $sector1->setColEnd(2) ;
        $sector1->setRowStart(1) ;
        $sector1->setRowEnd(2) ;

        $manager->persist($sector1);    

        $sector2 = new Sectors();

        $sector2->setNumber(2);
        $sector2->setColStart(2) ;
        $sector2->setColEnd(3) ;
        $sector2->setRowStart(1) ;
        $sector2->setRowEnd(2);

        $manager->persist($sector2);    

        $sector3 = new Sectors();

        $sector3->setNumber(3) ;
        $sector3->setColStart(3) ;
        $sector3->setColEnd(4) ;
        $sector3->setRowStart(1) ;
        $sector3->setRowEnd(2) ;

        $manager->persist($sector3); 

        $sector4 = new Sectors();

        $sector4->setNumber(4) ;
        $sector4->setColStart(4) ;
        $sector4->setColEnd(5) ;
        $sector4->setRowStart(1) ;
        $sector4->setRowEnd(2) ;

        $manager->persist($sector4); 

        $sector5 = new Sectors();


        $sector5->setNumber(5) ;
        $sector5->setColStart(5);
        $sector5->setColEnd(6) ;
        $sector5->setRowStart(1) ;
        $sector5->setRowEnd(2) ;

        $manager->persist($sector5);
        
        $sector6 = new Sectors();


        $sector6->setNumber(6) ;
        $sector6->setColStart(6) ;
        $sector6->setColEnd(7) ;
        $sector6->setRowStart(1) ;
        $sector6->setRowEnd(2) ;

        $manager->persist($sector6); 

        $sector7 = new Sectors();


        $sector7->setNumber(7) ;
        $sector7->setColStart(1) ;
        $sector7->setColEnd(2) ;
        $sector7->setRowStart(2) ;
        $sector7->setRowEnd(1) ;

        $manager->persist($sector7);     

        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
