<?php

namespace App\Repository;

use App\Entity\Reports;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Reports|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reports|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reports[]    findAll()
 * @method Reports[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReportsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reports::class);
    }

    public function findAllPending()
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.validated_by_scientist = :val')
            ->setParameter('val', false)
            ->orderBy('r.date', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function orderPendingByArea()
    {
            return $this->createQueryBuilder('r')
            ->andWhere('r.validated_by_scientist = :val')
            ->setParameter('val', false)
            ->orderBy('r.areas_id', 'ASC')        
            ->getQuery()
            ->getResult()
            ;
    
    }

    public function orderPendingByPriority()
    {
            return $this->createQueryBuilder('r')
            ->andWhere('r.validated_by_scientist = :val')
            ->setParameter('val', false)
            ->orderBy('r.priority', 'DESC')        
            ->getQuery()
            ->getResult()
            ;
    
    }

    // /**
    //  * @return Reports[] Returns an array of Reports objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Reports
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
