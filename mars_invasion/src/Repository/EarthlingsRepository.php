<?php

namespace App\Repository;

use App\Entity\Earthlings;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Earthlings|null find($id, $lockMode = null, $lockVersion = null)
 * @method Earthlings|null findOneBy(array $criteria, array $orderBy = null)
 * @method Earthlings[]    findAll()
 * @method Earthlings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EarthlingsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Earthlings::class);
    }

    // /**
    //  * @return Earthlings[] Returns an array of Earthlings objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Earthlings
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
