<?php

namespace App\Repository;

use App\Entity\Minerals;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Minerals|null find($id, $lockMode = null, $lockVersion = null)
 * @method Minerals|null findOneBy(array $criteria, array $orderBy = null)
 * @method Minerals[]    findAll()
 * @method Minerals[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MineralsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Minerals::class);
    }

    // /**
    //  * @return Minerals[] Returns an array of Minerals objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Minerals
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
