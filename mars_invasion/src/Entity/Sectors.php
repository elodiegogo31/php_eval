<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SectorsRepository")
 */
class Sectors
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $number;

    /**
     * @ORM\Column(type="smallint")
     */
    private $col_start;

    /**
     * @ORM\Column(type="smallint")
     */
    private $col_end;

    /**
     * @ORM\Column(type="smallint")
     */
    private $row_start;

    /**
     * @ORM\Column(type="smallint")
     */
    private $row_end;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Areas", mappedBy="sectors")
     */
    private $areas;

    public function __construct()
    {
        $this->areas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getColStart(): ?int
    {
        return $this->col_start;
    }

    public function setColStart(int $col_start): self
    {
        $this->col_start = $col_start;

        return $this;
    }

    public function getColEnd(): ?int
    {
        return $this->col_end;
    }

    public function setColEnd(int $col_end): self
    {
        $this->col_end = $col_end;

        return $this;
    }

    public function getRowStart(): ?int
    {
        return $this->row_start;
    }

    public function setRowStart(int $row_start): self
    {
        $this->row_start = $row_start;

        return $this;
    }

    public function getRowEnd(): ?int
    {
        return $this->row_end;
    }

    public function setRowEnd(int $row_end): self
    {
        $this->row_end = $row_end;

        return $this;
    }

    /**
     * @return Collection|Areas[]
     */
    public function getAreas(): Collection
    {
        return $this->areas;
    }

    public function addArea(Areas $area): self
    {
        if (!$this->areas->contains($area)) {
            $this->areas[] = $area;
            $area->setSectors($this);
        }

        return $this;
    }

    public function removeArea(Areas $area): self
    {
        if ($this->areas->contains($area)) {
            $this->areas->removeElement($area);
            // set the owning side to null (unless already changed)
            if ($area->getSectors() === $this) {
                $area->setSectors(null);
            }
        }

        return $this;
    }
}
