<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MineralsRepository")
 */
class Minerals
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $effects_on_population;

    /**
     * @ORM\Column(type="smallint")
     */
    private $threat_evaluation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Areas", inversedBy="minerals")
     */
    private $areas;

    public function __construct()
    {
        $this->areas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEffectsOnPopulation(): ?string
    {
        return $this->effects_on_population;
    }

    public function setEffectsOnPopulation(string $effects_on_population): self
    {
        $this->effects_on_population = $effects_on_population;

        return $this;
    }

    public function getThreatEvaluation(): ?int
    {
        return $this->threat_evaluation;
    }

    public function setThreatEvaluation(int $threat_evaluation): self
    {
        $this->threat_evaluation = $threat_evaluation;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return Collection|Areas[]
     */
    public function getAreas(): Collection
    {
        return $this->areas;
    }

    public function addArea(Areas $area): self
    {
        if (!$this->areas->contains($area)) {
            $this->areas[] = $area;
        }

        return $this;
    }

    public function removeArea(Areas $area): self
    {
        if ($this->areas->contains($area)) {
            $this->areas->removeElement($area);
        }

        return $this;
    }
}
