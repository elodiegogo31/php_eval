<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AreasRepository")
 */
class Areas
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $s_col_start;

    /**
     * @ORM\Column(type="smallint")
     */
    private $s_col_end;

    /**
     * @ORM\Column(type="smallint")
     */
    private $s_row_start;

    /**
     * @ORM\Column(type="smallint")
     */
    private $s_row_end;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_safe;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $has_ressources;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ground_type;

    /**
     * @ORM\Column(type="boolean")
     */
    private $has_been_investigated;

    /**
     * @ORM\Column(type="smallint")
     */
    private $risk;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sectors", inversedBy="areas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sectors;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reports", mappedBy="areas")
     */
    private $reports;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Minerals", mappedBy="areas")
     */
    private $minerals;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $picture;

    public function __construct()
    {
        $this->reports = new ArrayCollection();
        $this->minerals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSColStart(): ?int
    {
        return $this->s_col_start;
    }

    public function setSColStart(int $s_col_start): self
    {
        $this->s_col_start = $s_col_start;

        return $this;
    }

    public function getSColEnd(): ?int
    {
        return $this->s_col_end;
    }

    public function setSColEnd(int $s_col_end): self
    {
        $this->s_col_end = $s_col_end;

        return $this;
    }

    public function getSRowStart(): ?int
    {
        return $this->s_row_start;
    }

    public function setSRowStart(int $s_row_start): self
    {
        $this->s_row_start = $s_row_start;

        return $this;
    }

    public function getSRowEnd(): ?int
    {
        return $this->s_row_end;
    }

    public function setSRowEnd(int $s_row_end): self
    {
        $this->s_row_end = $s_row_end;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsSafe(): ?bool
    {
        return $this->is_safe;
    }

    public function setIsSafe(bool $is_safe): self
    {
        $this->is_safe = $is_safe;

        return $this;
    }

    public function getHasRessources(): ?bool
    {
        return $this->has_ressources;
    }

    public function setHasRessources(?bool $has_ressources): self
    {
        $this->has_ressources = $has_ressources;

        return $this;
    }

    public function getGroundType(): ?string
    {
        return $this->ground_type;
    }

    public function setGroundType(?string $ground_type): self
    {
        $this->ground_type = $ground_type;

        return $this;
    }

    public function getHasBeenInvestigated(): ?bool
    {
        return $this->has_been_investigated;
    }

    public function setHasBeenInvestigated(bool $has_been_investigated): self
    {
        $this->has_been_investigated = $has_been_investigated;

        return $this;
    }

    public function getRisk(): ?int
    {
        return $this->risk;
    }

    public function setRisk(int $risk): self
    {
        $this->risk = $risk;

        return $this;
    }

    public function getSectors(): ?Sectors
    {
        return $this->sectors;
    }

    public function setSectors(?Sectors $sectors): self
    {
        $this->sectors = $sectors;

        return $this;
    }

    /**
     * @return Collection|Reports[]
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Reports $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setAreas($this);
        }

        return $this;
    }

    public function removeReport(Reports $report): self
    {
        if ($this->reports->contains($report)) {
            $this->reports->removeElement($report);
            // set the owning side to null (unless already changed)
            if ($report->getAreas() === $this) {
                $report->setAreas(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Minerals[]
     */
    public function getMinerals(): Collection
    {
        return $this->minerals;
    }

    public function addMineral(Minerals $mineral): self
    {
        if (!$this->minerals->contains($mineral)) {
            $this->minerals[] = $mineral;
            $mineral->addArea($this);
        }

        return $this;
    }

    public function removeMineral(Minerals $mineral): self
    {
        if ($this->minerals->contains($mineral)) {
            $this->minerals->removeElement($mineral);
            $mineral->removeArea($this);
        }

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }
}
