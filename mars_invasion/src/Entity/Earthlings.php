<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EarthlingsRepository")
 * @UniqueEntity(
 *   fields={"genetic_fingerprint"},
 *   message= "Genetic fingerprint already in use."
 * )
 */
class Earthlings implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $age;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     */
    private $genetic_fingerprint;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Categories", inversedBy="earthlings")
     */
    private $categories;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Roles", inversedBy="earthlings")
     */
    private $roles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reports", mappedBy="earthlings", orphanRemoval=true)
     */
    private $reports;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->roles = new ArrayCollection();
        $this->reports = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(?int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getGeneticFingerprint(): ?string
    {
        return $this->genetic_fingerprint;
    }

    public function setGeneticFingerprint(string $genetic_fingerprint): self
    {
        $this->genetic_fingerprint = $genetic_fingerprint;

        return $this;
    }

    /**
     * @return Collection|Categories[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Categories $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Categories $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
        }

        return $this;
    }

    /**
     * @return Collection|Roles[]
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }

    public function addRole(Roles $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function removeRole(Roles $role): self
    {
        if ($this->roles->contains($role)) {
            $this->roles->removeElement($role);
        }

        return $this;
    }

    /**
     * @return Collection|Reports[]
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Reports $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setEarthlings($this);
        }

        return $this;
    }

    public function removeReport(Reports $report): self
    {
        if ($this->reports->contains($report)) {
            $this->reports->removeElement($report);
            // set the owning side to null (unless already changed)
            if ($report->getEarthlings() === $this) {
                $report->setEarthlings(null);
            }
        }

        return $this;
    }

    public function eraseCredentials() {}

    public function getSalt() {}


    public function getPassword()
    {
        return $this->genetic_fingerprint;

    }
    public function getUsername(){
        return $this->lastname;
    }


}
