<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoriesRepository")
 */
class Categories
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $special_traits;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Earthlings", mappedBy="categories")
     */
    private $earthlings;

    public function __construct()
    {
        $this->earthlings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSpecialTraits(): ?string
    {
        return $this->special_traits;
    }

    public function setSpecialTraits(string $special_traits): self
    {
        $this->special_traits = $special_traits;

        return $this;
    }

    /**
     * @return Collection|Earthlings[]
     */
    public function getEarthlings(): Collection
    {
        return $this->earthlings;
    }

    public function addEarthling(Earthlings $earthling): self
    {
        if (!$this->earthlings->contains($earthling)) {
            $this->earthlings[] = $earthling;
            $earthling->addCategory($this);
        }

        return $this;
    }

    public function removeEarthling(Earthlings $earthling): self
    {
        if ($this->earthlings->contains($earthling)) {
            $this->earthlings->removeElement($earthling);
            $earthling->removeCategory($this);
        }

        return $this;
    }
}
