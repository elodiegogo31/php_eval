<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReportsRepository")
 */
class Reports
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="boolean")
     */
    private $validated_by_scientist;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Earthlings", inversedBy="reports")
     * @ORM\JoinColumn(nullable=false)
     */
    private $earthlings;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Areas", inversedBy="reports")
     * @ORM\JoinColumn(nullable=false)
     */
    private $areas;

    /**
     * @ORM\Column(type="smallint")
     */
    private $priority;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getValidatedByScientist(): ?bool
    {
        return $this->validated_by_scientist;
    }

    public function setValidatedByScientist(bool $validated_by_scientist): self
    {
        $this->validated_by_scientist = $validated_by_scientist;

        return $this;
    }

    public function getEarthlings(): ?Earthlings
    {
        return $this->earthlings;
    }

    public function setEarthlings(?Earthlings $earthlings): self
    {
        $this->earthlings = $earthlings;

        return $this;
    }

    public function getAreas(): ?Areas
    {
        return $this->areas;
    }

    public function setAreas(?Areas $areas): self
    {
        $this->areas = $areas;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }
}
