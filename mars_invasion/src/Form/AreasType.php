<?php

namespace App\Form;

use App\Entity\Areas;
use App\Entity\Sectors;
use App\Entity\Minerals;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType; 

class AreasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('s_col_start',IntegerType::class)
            ->add('s_col_end',IntegerType::class)
            ->add('s_row_start',IntegerType::class)
            ->add('s_row_end',IntegerType::class)
            ->add('name',TextType::class)
            ->add('is_safe', ChoiceType::class, [
                'choices'  => [
                    'Yes' => true,
                    'No' => false,
                ],
            ])
            ->add('has_ressources', ChoiceType::class, [
                'choices'  => [
                    'Yes' => true,
                    'No' => false,
                ],
            ])
            ->add('ground_type',TextType::class)
            ->add('has_been_investigated', ChoiceType::class, [
                'choices'  => [
                    'Yes' => true,
                    'No' => false,
                ],
            ])
            ->add('risk',IntegerType::class)
            ->add('sectors', EntityType::class,[
                'class' => Sectors::class,
                'choice_label' => 'number'
            ])
            ->add('minerals', EntityType::class,[
                'class' => Minerals::class,
                'choice_label' => 'name',
                'multiple' => true
            ])
            ->add('picture',TextType::class)

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Areas::class,
        ]);
    }
}
