<?php

namespace App\Controller;

use App\Entity\Minerals;
use App\Form\MineralsType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MineralsController extends AbstractController
{
     /**
     * @Route("/minerals/list", name="list_minerals")
     */
    public function index( Request $request)
    {
        $mineralsRepo= $this->getDoctrine()->getRepository(Minerals::class);
        $minerals= $mineralsRepo->findall();
       // dd($minerals);

        return $this->render('minerals/minerals.html.twig', [
            'minerals' => $minerals
        ]);
    }

    /**
     * @Route("/minerals/add", name="minerals_add")
     */
    public function add( Request $request)
    {
        $mineral= new Minerals();
        $manager= $this->getDoctrine()->getManager();
        $form = $this->createForm(MineralsType::class, $mineral);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($mineral);
            $manager->flush();
            $this->addFlash('success', 'Your discovery has been registered.');


            return $this->redirectToRoute('list_minerals');
        }
        return $this->render('minerals/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
