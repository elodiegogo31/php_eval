<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Sectors;
use App\Entity\Areas;


class SectorsController extends AbstractController
{
    /**
     * @Route("/sectors", name="sectors")
     */
    public function index()
    {
        $sectorRepo = $this->getDoctrine()->getRepository(Sectors::class);
        $sectors= $sectorRepo->findAll();
       // dd($sectors);
        return $this->render('sectors/index.html.twig', [
            'sectors' => $sectors
        ]);
    }

    /**
     * @Route("/sectors/show/{id}", name="show_sector")
     */
    public function show($id)
    {
        $sectorRepo = $this->getDoctrine()->getRepository(Sectors::class);
        $sector_targeted= $sectorRepo->find($id);
        $areasRepo = $this->getDoctrine()->getRepository(Areas::class);
        $areas_of_sector_targeted = $areasRepo->findBySectors($sector_targeted);
        //dd($areas_of_sector_targeted);

       // dd($sector_targeted);
        return $this->render('sectors/sector.html.twig', [
            'areas_of_sector_targeted' => $areas_of_sector_targeted,
            'sector_targeted' => $sector_targeted
        ]);
    }
}
