<?php

namespace App\Controller;

use App\Entity\Areas;
use App\Entity\Reports;
use App\Form\ReportsType;
use App\Entity\Earthlings;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ReportsController extends AbstractController
{
    /**
     * @Route("/reports", name="list_reports")
     */
    public function index()
    {
        $reportsRepo = $this->getDoctrine()->getRepository(Reports::class);
        $reports= $reportsRepo->findAllPending();
       // dd($reports;

        return $this->render('reports/index.html.twig', [
            'reports' => $reports
        ]);
    }

      /**
     * @Route("/reports/edit/{id}", name="edit_report")
     */
    public function edit($id)
    {
        $reportsRepo = $this->getDoctrine()->getRepository(Reports::class);
        $report_targeted= $reportsRepo->find($id);
        $report_targeted->setValidatedByScientist(true);
        $this->getDoctrine()->getManager()->flush();
       //dd($report_targeted);
       
        return $this->redirectToRoute('list_reports');
    }

      /**
     * @Route("/reports/delete/{id}", name="delete_report")
     */
    public function delete($id)
    {
        $reportsRepo = $this->getDoctrine()->getRepository(Reports::class);
        $report_targeted= $reportsRepo->find($id);
        $this->getDoctrine()->getManager()->remove($report_targeted);
        $this->getDoctrine()->getManager()->flush();
       
        return $this->redirectToRoute('list_reports');
    }
     /**
     * @Route("/reports/orderByArea", name="report_orderByArea")
     */

    public function orderByArea()
    {
        $reportsRepo = $this->getDoctrine()->getRepository(Reports::class);
        $reports_sorted= $reportsRepo->orderPendingByArea();
        return $this->render('reports/index.html.twig', [
            'reports' => $reports_sorted
        ]);
    }

      /**
     * @Route("/reports/orderByPriority", name="report_orderByPriority")
     */

    public function orderByPriority()
    {
        $reportsRepo = $this->getDoctrine()->getRepository(Reports::class);
        $reports_sorted= $reportsRepo->orderPendingByPriority();
        return $this->render('reports/index.html.twig', [
            'reports' => $reports_sorted
        ]);
    }

   /**
     * @Route("/reports/create/{id}", name="report_create")
     */

    public function create(Request $request, $id)
    {
        $earthlingsRepo= $this->getDoctrine()->getRepository(Earthlings::class);
         /////////////// Bloque sur user1, quand auth marchera chopper via session  ////////////////////
        $earthlingslogged= $earthlingsRepo->find(1);
        /////////////// Bloque sur user1, quand auth marchera chopper via session  ////////////////////

        $areasRepo= $this->getDoctrine()->getRepository(Areas::class);
        $reportGeneratedForArea = $areasRepo->find($id);

        $report= new Reports();
        $manager= $this->getDoctrine()->getManager();
        $form = $this->createForm(ReportsType::class, $report);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $report->setDate(new \Datetime());
            $report->setAreas( $reportGeneratedForArea );
            $report->setEarthlings($earthlingslogged);/////////////// Bloque sur user1, quand auth marchera chopper via session  ////////////////////
            $report->setValidatedByScientist(false);




            $manager->persist($report);
            $manager->flush();
            $this->addFlash('success', 'Your report has been sent to tour team of scientists');

        }
        return $this->render('reports/create.html.twig', [
            'form' => $form->createView()
        ]);



    }



}
