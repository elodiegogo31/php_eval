<?php

namespace App\Controller;

use App\Entity\Areas;
use App\Entity\Sectors;
use App\Form\AreasType;
use App\Entity\Minerals;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AreasController extends AbstractController
{
    /**
     * @Route("/areas/add", name="areas_add")
     */
    public function index( Request $request )
    {
        $area= new Areas();
        $manager= $this->getDoctrine()->getManager();
        $form = $this->createForm(AreasType::class, $area);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($area);
            $manager->flush();
            $this->addFlash('success', "A new area has been added.");

        }
        return $this->render('areas/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/areas/show/{id}", name="areas_show")
     */
    public function show($id)
    {
    
       // $sectorRepo = $this->getDoctrine()->getRepository(Sectors::class);
       // $sector_targeted= $sectorRepo->find($id);
       // $areasRepo = $this->getDoctrine()->getRepository(Areas::class);
       // $areas_of_sector_targeted = $areasRepo->findBySectors($sector_targeted);


        $sectorRepo = $this->getDoctrine()->getRepository(Sectors::class);
        $areasRepo = $this->getDoctrine()->getRepository(Areas::class);
        $mineralsRepo = $this->getDoctrine()->getRepository(Minerals::class);


        $area= $areasRepo->find($id);
        $sectorOfArea = $area->getSectors()->getId();
        //dd($sectorOfArea);
        $sectorInfo = $sectorRepo->find($sectorOfArea);

        //$area_minerals= $area->getMinerals();
        //dd($area_minerals);
       // dd($area->getMinerals());

       /*$areaMinerals= $area->getMinerals();
        $listOfMineralsEntities = [];
        foreach( $areaMinerals as $mineral){

        $area_minerals= $areasRepo->findByName($mineral);
        $listOfMineralsEntities[] = $area_minerals;

        }
        dd($listOfMineralsEntities);
*/




       /* $areaRepo= $this->getDoctrine()->getRepository(Areas::class);
        $area_selected= $areaRepo->find($id);
        //dd($area_selected);

        $mineralRepo= $this->getDoctrine()->getRepository(Minerals::class);
        $area_selected_minerals= $area_selected->getMinerals();
       // dd($area_selected_minerals);
       // dd($area_selected_minerals['collection']['elements']);

        $mineralEntitiesNeeded= [];
        foreach($area_selected_minerals as $mineral)
        {
            $mineral_searchedBy_name= $mineralRepo->findByName($mineral);
            $mineralEntitiesNeeded[] = $mineral_searchedBy_name;
        }
        //dd($mineralEntitiesNeeded);
        
        return $this->render('areas/show.html.twig', [
            'areas_of_sector_targeted' => $areas_of_sector_targeted ,
            'sector_targeted' => $sector_targeted ,

            'minerals' => $mineralEntitiesNeeded,
            'area' => $area_selected
        ]);


            */
        return $this->render('areas/show.html.twig', [
            'areaInfo' => $area ,
            'sectorInfo' => $sectorInfo 

        ]);
    }
}
