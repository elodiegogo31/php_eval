<?php

namespace App\Controller;

use App\Entity\Earthlings;
use App\Form\EarthlingsType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class EarthlingsController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $earthling= new Earthlings();
        $manager= $this->getDoctrine()->getManager();
        $form = $this->createForm(EarthlingsType::class, $earthling);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $hash= $encoder->encodePassword($earthling, $earthling->getGeneticFingerprint());
            $earthling->SetGeneticFingerprint($hash);
            $manager->persist($earthling);
            $manager->flush();
            $this->addFlash('success', 'Your account has been successfully created');

            return $this->redirectToRoute('login');
        }

        return $this->render('earthlings/register.html.twig', [
            'form' => $form->createView()
            
        ]);
    }
}
